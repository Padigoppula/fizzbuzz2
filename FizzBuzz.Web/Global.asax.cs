﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using FizzBuzz.Web.Repositories;
using FizzBuzz.Web.MVCAddOns;
using FizzBuzz.Web.Helpers;
using System.Diagnostics;
using FizzBuzz.Web.Controllers;
namespace FizzBuzz.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
         /// <summary>
        /// Handles the Init event of the Application control.
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //MvcApplication.RegisterUnityContainer();
            BuildUnityContainer();
        }

        private static void RegisterUnityContainer()
        {
            IUnityContainer diContainer = new UnityContainer();
            diContainer.RegisterType<IFizzBuzzRepository, FizzBuzzRepository>();

            CustomControllerFactory factory
                = new CustomControllerFactory(diContainer);

            ControllerBuilder.Current.SetControllerFactory(factory);
        }


        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            container.RegisterType<IFizzBuzzRepository, FizzBuzzRepository>();
            container.RegisterType<IController, FizzBuzzController>();
            DependencyResolver.SetResolver(new Unity.Mvc4.UnityDependencyResolver(container));
            
            return container;
        }
               /// <summary>
        /// Handles all exceptions thrown in this application
        /// Logs the exception and redirects user to a friendly error page
        /// </summary>
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            ExceptionHandler.LogEntry(EventLogEntryType.Error, ex, 1001);
        }
    }
}
