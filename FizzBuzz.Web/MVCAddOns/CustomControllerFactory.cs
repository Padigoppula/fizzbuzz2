﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using System.Web.Mvc;

namespace FizzBuzz.Web.MVCAddOns
{
    public class CustomControllerFactory : DefaultControllerFactory
    {
        private readonly IUnityContainer diContainer;
        public CustomControllerFactory(IUnityContainer diContainer)
        {
            this.diContainer = diContainer;
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
                return null;
            return this.diContainer.Resolve(controllerType) as IController;
        }
    }
}