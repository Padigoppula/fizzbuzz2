﻿var ApiHelper = new function () {
    this.callPostAPI = function (url, content) {
        var inputData = { 'val': content };
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "html",
            data: inputData,
            async: true,
            cache: false,
            success: function (result) {
                $("#divResult").html(result);
                console.log("Success in ApiHelper calling: " + url + "; result: " + result);
            },
            error: function (result) {
                console.log("Error in ApiHelper calling: " + url + "; result: " + result);
            }
        });
    };

   
}