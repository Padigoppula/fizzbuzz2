﻿/// <reference path="apiHelper.js" />

// RTAM Server APIs
var rtamServerAPI = {
    update: "FizzBuzz/GetCalculatedResult",
};


function Calculate() {
    var input = $("#txtNumber").val()

    if (isNaN(input)) {
        alert('Please enter numbers only');
        return false;
    }
    else if ((input < 1) || input > 1000) {
        alert('Please enter between  1 to 1000');
        return false;
    }
    ApiHelper.callPostAPI(rtamServerAPI.update, input)
    
}