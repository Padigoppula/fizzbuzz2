﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.Web.Repositories;
using FizzBuzz.Model;
using FizzBuzz.Web.Helpers;
namespace FizzBuzz.Web.Controllers
{
    public class FizzBuzzController : Controller
    {
        private IFizzBuzzRepository _fizbuzz;

        #region Constructor
        public FizzBuzzController(IFizzBuzzRepository fizzBuzz)
        {
            this._fizbuzz= fizzBuzz;
        }
        #endregion

        #region public methods
        
        public ActionResult Index()
        {
            return View(Constants.ViewName.index);
        }

        public PartialViewResult GetCalculatedResult(int val)
        {
            //Submit the data to API
            this._fizbuzz.Submit(val);
            //calculate the input val
            IList<Quiz> quizList = this._fizbuzz.GetResult(val);
            //Return partial view
            //return PartialView("~/Views/FizzBuzz/_Status.cshtml", quizList);
            return PartialView(Constants.ViewName.searchResult, quizList);
        }
        #endregion
    }
}