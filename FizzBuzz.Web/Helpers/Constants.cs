﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
namespace FizzBuzz.Web.Helpers
{
    public class Constants
    {
        public const string wednesDay = "wednesday";
        public const string fizz = "fizz";
        public const string buzz = "buzz";
        public const string fizzbuzz = "fizz buzz";
        public const string wizz = "wizz";
        public const string wuzz = "wuzz";
        public const string wizzwuzz = "wizz wuzz";

        /* Event log constants*/
        public const string sSource = "dotNET Assessment";
        public const string sLog = "Application";
        public const string sEvent = "Sample Event";
        public static string APIUrl = ConfigurationManager.AppSettings["RestUrl"];
        public const string apiFormater = "{0}?val={1}";


        public class ViewName
        {
            public static readonly string index = "Index";
            public static readonly string searchResult = "_SearchResult";
        }
    }
}