﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace FizzBuzz.Web.Helpers
{
    public class ApiHelper
    {
        public static async Task<string> CallAPI(string uri)
        {
            var result = "{}";
            try
            {
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(uri);
                    //Call API method
                    var response = await client.GetAsync(uri).ConfigureAwait(continueOnCapturedContext: false);
                    if (response.IsSuccessStatusCode)
                    {
                        
                            string successMessage = String.Format("Intserted data to API call successfully: {0}.", uri);
                            ExceptionHandler.LogEntry(EventLogEntryType.Information, successMessage, 31015);
                    }
                    else
                    {
                        string responseCode = (response != null) ? response.StatusCode.ToString() : string.Empty;
                        string errorMessage = String.Format("Error while GET on uri: {0}. Response code: {1}", uri, responseCode);
                        ExceptionHandler.LogEntry(EventLogEntryType.Error, errorMessage, 31006);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.LogEntry(EventLogEntryType.Error, ex, 31007);
            }

            return result;
        }
    }
}