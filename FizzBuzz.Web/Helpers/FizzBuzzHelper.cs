﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FizzBuzz.Model;
namespace FizzBuzz.Web.Helpers
{
    public static class FizzBuzzHelper
    {
        #region public methods
        /// <summary>
        /// Calculate the input string
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static IList<Quiz> Calculate(int val)
        {
            List<Quiz> list = new List<Quiz>();
            //return empty if the input val is empty
            if (!Isvalid(val))
                return list;
            for (int i = 1; i < val; i++)
            {
                //check if the input is divisible by 3 and 5
                if (i % 3 == 0 && i % 5 == 0)
                {
                    list.Add(new Quiz { QuizName = !isTodayWednessDay() ? Constants.fizzbuzz : Constants.wizzwuzz });
                }
                //Check if the input is divisible by 3
                else if (i % 3 == 0)
                {
                    list.Add(new Quiz { QuizName = !isTodayWednessDay() ? Constants.fizz : Constants.wizz });
                }
                //Check if the input is divisible by 5
                else if (i % 5 == 0)
                {
                    list.Add(new Quiz { QuizName = !isTodayWednessDay() ? Constants.buzz : Constants.wuzz });
                }
                else
                {
                    list.Add(new Quiz { QuizName = i.ToString() });
                }
            }
            return list;
        }

        public static void SubmitData(int val)
        {
            var uri = string.Format(Constants.apiFormater, Constants.APIUrl, val);
            //Call the API
            ApiHelper.CallAPI(uri).ConfigureAwait(continueOnCapturedContext: false);
        }
        #endregion
        #region Private methods
        /// <summary>
        /// To Validate the input is with in the Range
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private static bool Isvalid(int val)
        {
            bool flag = false;
            if (val == null)
            {
                flag = false;
                return flag;
            }
            else if (val >= 0 && val <= 1000)
            {
                flag = true;
            }
            return flag;
        }
        /// <summary>
        /// To check if current date is wednesday 
        /// </summary>
        /// <returns></returns>
        private static bool isTodayWednessDay()
        {
            bool isWednessDay = false;
            var Day = DateTime.Now.DayOfWeek.ToString();
            if (Day.ToLowerInvariant() == Constants.wednesDay.ToLowerInvariant())
                isWednessDay = true;
            return isWednessDay;
        }
        #endregion
    }
}