﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using FizzBuzz.Web.Helpers;
namespace FizzBuzz.Web.Helpers
{
    public class ExceptionHandler
    {
        /// <summary>
        /// Logs the entry.
        /// </summary>
        /// <param name="type">The event entry type.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="eventId">The event id.</param>
        public static void LogEntry(EventLogEntryType type, Exception exception, int eventId)
        {
            if (!EventLog.SourceExists( Constants.sSource))
                EventLog.CreateEventSource(Constants.sSource, Constants.sLog);

            EventLog.WriteEntry(Constants.sSource, exception.Message, type, eventId);
        }

        /// <summary>
        /// Logs the entry.
        /// </summary>
        /// <param name="type">The event entry type.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="eventId">The event id.</param>
        public static void LogEntry(EventLogEntryType type, string exception, int eventId)
        {
            if (!EventLog.SourceExists(Constants.sSource))
                EventLog.CreateEventSource(Constants.sSource, Constants.sLog);

            EventLog.WriteEntry(Constants.sSource, exception, type, eventId);
        }
    }
}