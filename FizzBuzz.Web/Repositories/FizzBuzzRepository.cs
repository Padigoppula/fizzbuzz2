﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FizzBuzz.Web.Repositories;
using FizzBuzz.Web.Helpers;
using FizzBuzz.Model;
namespace FizzBuzz.Web.Repositories
{
    public class FizzBuzzRepository : IFizzBuzzRepository
    {
       /// <summary>
       /// Get the result for corresponding input
       /// </summary>
       /// <param name="val"></param>
       /// <returns></returns>
        public IList<Quiz> GetResult(int val)
        {
            //Calculate input
           return FizzBuzzHelper.Calculate(val);
        }

        /// <summary>
        /// Submit the input
        /// </summary>
        /// <param name="val"></param>
        public void Submit(int val)
        {
            //Submit data to API
            FizzBuzzHelper.SubmitData(val);
        }


    }
}