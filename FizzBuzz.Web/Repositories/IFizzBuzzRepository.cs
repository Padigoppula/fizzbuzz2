﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FizzBuzz.Model;
namespace FizzBuzz.Web.Repositories
{
    public interface IFizzBuzzRepository
    {
        IList<Quiz> GetResult(int val);
        void Submit(int val);
    }
}