﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using FizzBuzz.Web.Repositories;
using FizzBuzz.Web.Controllers;
using System.Web.Mvc;
using FizzBuzz.Model;
using System.Linq;
namespace FizzBuzz.Web.Test.Controllers
{
    /// <summary>
    /// Summary description for FizzBuzzControllerTest
    /// </summary>
    [TestClass]
    public class FizzBuzzControllerTest
    {
        private const string viewNameSearchResult = "_SearchResult";
        private const string viewNameIndex = "Index";
        private Mock<IFizzBuzzRepository> _quizRepositorymock;

        [TestInitialize]
        public void Initialize()
        {
            _quizRepositorymock = new Mock<IFizzBuzzRepository>();
        }

        [TestMethod()]
        public void Index_Test()
        {

            var FizzBuzzController = new FizzBuzzController(_quizRepositorymock.Object);
            ViewResult result = FizzBuzzController.Index() as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewName.ToLowerInvariant().Contains(viewNameIndex.ToLowerInvariant()));
        }

        [TestMethod]
        public void Update_Returns_ViewName_test()
        {
            List<Quiz> lst= new List<Quiz>();
            lst.Add(new Quiz { QuizName = "Fizz" });
            lst.Add(new Quiz { QuizName = "Buzz" });
            _quizRepositorymock.Setup(s => s.GetResult(It.IsAny<int>())).Returns(lst);
            _quizRepositorymock.Setup(x => x.Submit(It.IsAny<int>()));
            var FizzBuzzController = new FizzBuzzController(_quizRepositorymock.Object);
            var result = FizzBuzzController.GetCalculatedResult(It.IsAny<int>()) as PartialViewResult;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewName.ToLowerInvariant().Contains(viewNameSearchResult.ToLowerInvariant()));
        }

        [TestMethod]
        public void Update_Returns_Model_test()
        {
            List<Quiz> lst = new List<Quiz>();
            lst.Add(new Quiz { QuizName = "Fizz" });
            lst.Add(new Quiz { QuizName = "Buzz" });
            _quizRepositorymock.Setup(s => s.GetResult(It.IsAny<int>())).Returns(lst);
            _quizRepositorymock.Setup(x => x.Submit(It.IsAny<int>()));
            var FizzBuzzController = new FizzBuzzController(_quizRepositorymock.Object);
            var result = FizzBuzzController.GetCalculatedResult(It.IsAny<int>()) as PartialViewResult;

            Assert.IsNotNull(result.Model);
            Assert.AreEqual(((List<Quiz>)result.Model).Count(), 2 );
            
        }
    }
}
