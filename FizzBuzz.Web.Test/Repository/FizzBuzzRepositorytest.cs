﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.Web.Repositories;
using Moq;
using System.Collections.Generic;
using FizzBuzz.Model;
namespace AssessmentWebTest.Repository
{
    [TestClass]
    public class FizzBuzzRepositorytest
    {
        public IFizzBuzzRepository _homeRepositorymock;

        [TestInitialize]
        public void Initialize()
        {
            //Instantiate object
            _homeRepositorymock = new FizzBuzzRepository();
        }

        [TestMethod]
        public void Calculate_test()
        {
            int val = 5;
            
            //Calling the method
            var lst = _homeRepositorymock.GetResult(val);

            Assert.IsNotNull(lst);
            Assert.IsTrue(lst.Count > 0);
        }
    }
}
